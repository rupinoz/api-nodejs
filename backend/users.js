/**
 * @author Dimas Erlangga Putera
 * @version 1.0.0
 * @since 25/08/2020
 */

const knex = require('../tools/knex');
const help = require('../tools/helper');

function getUser() {
    return new Promise((resolve, reject) => {
        try {
            knex('tbl_user').select('*').then((result) => {
                    if (result.length > 0) {
                        resolve({
                            status: "Success",
                            code: "00",
                            data: result
                        });
                    }
                }).catch((error) => {
                reject(error.message);
            });
        } catch(error){
            reject(error.message);
        }
    });
}

function postUser(data) {
    return new Promise((resolve, reject) => {
        try {
            knex('tbl_user').select('*')
                .where('user',data.user)
                .then((result) => {
                if (result.length > 0) {
                    resolve({
                        status: "Success",
                        code: "00",
                        data: result
                    });
                }else{
                    resolve({
                        status: "Failed",
                        code: "R3",
                        data: {
                            message: "Data Not Found"
                        }
                    });
                }
            }).catch((error) => {
                reject(error.message);
            });
        } catch(error){
            reject(error.message);
        }
    });
}

function putUser(data) {
    return new Promise((resolve, reject) => {
        try {
            if(!data.user || !data.password || !data.name || !data.status){
                reject("F2");
            }else{
                let dataInsert = {
                    user: data.user,
                    password: help.hashMd5(data.password),
                    name: data.name,
                    status: data.status
                };

                knex.insert(dataInsert).into("tbl_user").then(() => {
                    resolve({
                        status: "Success",
                        code: "00",
                        data: {
                            message: "Data Inserted"
                        }
                    });
                }).catch((error) => {
                    reject(error.message);
                });
            }
        } catch(error){
            reject(error.message);
        }
    });
}

function deleteUser(data) {
    return new Promise((resolve, reject) => {
        try {
            if(!data.user || data.user === ""){
                reject("F2");
            }else{
                knex('tbl_user').where('user', data.user).del().then((result) => {
                    if(result > 0){
                        resolve({
                            status: "Success",
                            code: "00",
                            data: {
                                message: "Data Deleted"
                            }
                        });
                    }else{
                        reject("F2");
                    }
                }).catch((error) => {
                    reject(error.message);
                });
            }
        } catch(error){
            reject(error.message);
        }
    });
}

module.exports = {
    getUser, postUser, putUser, deleteUser
};