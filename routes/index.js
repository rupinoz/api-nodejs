var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.status(200);
  res.send({
    "status":"Success",
    "code":"00",
    "data":{
      "service":"Rest API NodeJS Ready",
      "author":"Rupinoz",
    }
  });
});

module.exports = router;
