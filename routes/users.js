/**
 * @author Dimas Erlangga Putera
 * @version 1.0.0
 * @since 25/08/2020
 */

const express = require('express');
const router = express.Router();
const mod = require('../backend/users');

let errorResp = {
  status: "Error",
  code: "R1",
  data: {
    message: "Backend Error"
  }
};

let failedResp = {
  status: "Failed",
  code: "F2",
  data: {
    message: "Data Failed"
  }
};

router.get('/', function(req, res) {
  mod.getUser().then((response) => {
    res.status(200);
    res.json(response);
  }).catch((error) => {
    console.log(error);
    res.status(500);
    res.json(errorResp);
  });
});

router.post('/', function(req, res) {
  let data = {
    user: req.body.user
  };

  mod.postUser(data).then((response) => {
    res.status(200);
    res.json(response);
  }).catch((error) => {
    console.log(error);
    res.status(500);
    res.json(errorResp);
  });
});

router.put('/', function(req, res) {
  let data = {
    user: req.body.user,
    password: req.body.password,
    name: req.body.name,
    status: req.body.status
  };

  mod.putUser(data).then((response) => {
    res.status(200);
    res.json(response);
  }).catch((error) => {
    console.log(error);
    if(error === "F2"){
      res.status(400);
      res.json(failedResp);
    }else{
      res.status(500);
      res.json(errorResp);
    }
  });
});

router.delete('/', function(req, res) {
  let data = {
    user: req.body.user
  };

  mod.deleteUser(data).then((response) => {
    res.status(200);
    res.json(response);
  }).catch((error) => {
    console.log(error);
    if(error === "F2"){
      res.status(400);
      res.json(failedResp);
    }else{
      res.status(500);
      res.json(errorResp);
    }
  });
});

module.exports = router;
