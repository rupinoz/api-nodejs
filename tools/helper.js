/**
 * @author Dimas Erlangga Putera
 * @version 1.0.0
 * @since 11/06/2020
 */

const sha256 = require('sha256');
const md5 = require('md5');
const base64 = require('base-64');
const crypto = require('crypto');
const jwt = require("jsonwebtoken");
const dateTime = require('node-datetime');

let getUniqId = ()=>{
    let dt = dateTime.create();
    let rand = Math.floor(Math.random() * Math.floor(1000));
    let formatted = dt.format('ymdHMS');
    return `${formatted}${rand}`;
};

let getUniqVa = ()=>{
    let dt = dateTime.create();
    let rand = Math.floor(Math.random() * Math.floor(1000));
    let formatted = dt.format('M');
    return `${formatted}${rand}`;
};

let hashMd5 = (value)=>{
    let hash = md5(`${value}`);
    return `${hash}`;
};

let hashSha256 = (value)=>{
    let hash = sha256(`${value}`);
    return `${hash}`;
};

let hashSha256Base64 = (value)=>{
    let hash = crypto.createHash('sha256').update(`${value}`).digest('base64');
    return `${hash}`;
};

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(Buffer.from(base64, 'base64').toString().split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

function jwtHash(payload,secret){
    let token = jwt.sign(payload, secret);
    return `${token}`;
}

module.exports = {
    getUniqId, getUniqVa, hashSha256, hashSha256Base64, hashMd5, pad, parseJwt, jwtHash
};