/**
 * @author Dimas Erlangga Putera
 * @version 1.0
 * @since 11/06/2020
 */

const mysql = require('mysql');
const knex = require('knex')({
    client: 'mysql',
    connection: {
        host     : process.env.DB_HOST,
        user     : process.env.DB_UNAME,
        password : process.env.DB_PASS,
        database : process.env.DB_NAME,
        port     : process.env.DB_PORT
    },
    pool: { min: 0, max: parseInt(process.env.DB_POOL) }
});

module.exports = knex;
